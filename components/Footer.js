import Link from "next/link";
import Image from "next/image";
import { useState } from "react";

const Footer = () => {
  const [newsLetterError, setNewsLetterError] = useState();
  return (
    <footer className="footer">
      <div className="container">
        <div className="footer__logo">
          <Image
            src="/images/logo.svg"
            alt="manage logo"
            width={146}
            height={24}
          />

          <div className="social__icons">
            <Link href="/">
              <a>
                <Image
                  src="/images/icon-facebook.svg"
                  alt="facebook link"
                  width={20 * 1.4}
                  height={20 * 1.4}
                />
              </a>
            </Link>
            <Link href="/">
              <a>
                <Image
                  src="/images/icon-youtube.svg"
                  alt="youtube link"
                  width={21 * 1.4}
                  height={20 * 1.4}
                />
              </a>
            </Link>
            <Link href="/">
              <a>
                <Image
                  src="/images/icon-twitter.svg"
                  alt="twitter link"
                  width={20 * 1.4}
                  height={18 * 1.4}
                />
              </a>
            </Link>
            <Link href="/">
              <a>
                <Image
                  src="/images/icon-pinterest.svg"
                  alt="pinterest link"
                  width={20 * 1.4}
                  height={20 * 1.4}
                />
              </a>
            </Link>
            <Link href="/">
              <a>
                <Image
                  src="/images/icon-instagram.svg"
                  alt="instagram link"
                  width={21 * 1.4}
                  height={20 * 1.4}
                />
              </a>
            </Link>
          </div>
        </div>

        <div className="footer__links">
          <div className="footer__links__container">
            <Link href="/">
              <a className="footer__link">Home</a>
            </Link>
            <Link href="/">
              <a className="footer__link">Pricing</a>
            </Link>
            <Link href="/">
              <a className="footer__link">Products</a>
            </Link>
            <Link href="/">
              <a className="footer__link">About Us</a>
            </Link>
          </div>

          <div className="footer__links__container">
            <Link href="/">
              <a className="footer__link">Careers</a>
            </Link>
            <Link href="/">
              <a className="footer__link">Community</a>
            </Link>
            <Link href="/">
              <a className="footer__link">Privacy Policy</a>
            </Link>
          </div>
        </div>

        <div className="footer__newsletter">
          <form
            onSubmit={(e) => {
              e.preventDefault();
              setNewsLetterError();
            }}
            className="form"
            onInvalid={(e) => {
              e.preventDefault();
              e.target.setCustomValidity("");
              if (!e.target.validity.valid) {
                e.target.setCustomValidity("");
              }
            }}
          >
            <input
              className="form-input"
              placeholder="Updates in your inbox..."
              type="email"
              required
              onInvalid={() =>
                setNewsLetterError("Please insert a valid email")
              }
            />
            <em className="error">{newsLetterError}</em>
            <button className="btn" type="submit">
              Go
            </button>
          </form>
        </div>
      </div>

      <div className="container">
        <div className="copyright">Copyright 2020. All Rights Reserved</div>
      </div>

      <div className="attribution">
        Challenge by{" "}
        <Link href="//frontendmentor.io?ref=challenge">
          <a target="_blank" rel="noreferrer" referrerPolicy="no-referrer">
            Frontend Mentor
          </a>
        </Link>
        . Coded by{" "}
        <Link href="//github.com/oreoluwa-bs">
          <a target="_blank" rel="noreferrer" referrerPolicy="no-referrer">
            Ore Bimbo-Salami
          </a>
        </Link>
        .
      </div>
    </footer>
  );
};

export default Footer;
