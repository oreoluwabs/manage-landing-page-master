import Link from "next/link";
import PropTypes from "prop-types";

const GetStartedButton = ({ href = "/", className = "" }) => {
  return (
    <Link href={href}>
      <a className={`${className} get-started__btn`}>Get Started</a>
    </Link>
  );
};

export default GetStartedButton;

GetStartedButton.propTypes = {
  href: PropTypes.string,
  className: PropTypes.string,
};
