import Link from "next/link";
import Image from "next/image";
import { useState } from "react";
import GetStartedButton from "./Button/GetStartedButton";

const NavLinks = () => {
  return (
    <span>
      <Link href="/">
        <a className="nav__item">Pricing</a>
      </Link>
      <Link href="/">
        <a className="nav__item">Product</a>
      </Link>
      <Link href="/">
        <a className="nav__item">About Us</a>
      </Link>
      <Link href="/">
        <a className="nav__item">Careers</a>
      </Link>
      <Link href="/">
        <a className="nav__item">Comunity</a>
      </Link>
    </span>
  );
};

const Navbar = () => {
  const [drawerState, setDrawerState] = useState(false);

  return (
    <>
      <header className="header container">
        <Image
          src="/images/logo.svg"
          width={146}
          height={24}
          alt="manage logo"
        />
        <div
          className="header__menu-icon"
          onClick={() => setDrawerState(!drawerState)}
        >
          {!drawerState && (
            <Image
              src="/images/icon-hamburger.svg"
              width={25}
              height={18}
              alt="open drawer"
            />
          )}
          {drawerState && (
            <Image
              src="/images/icon-close.svg"
              width={21}
              height={22}
              alt="close drawer"
            />
          )}
        </div>

        <nav
          className="header__nav-mobile"
          style={{
            minHeight: drawerState ? "100vh" : 0,
            opacity: drawerState ? 1 : 0,
          }}
        >
          <NavLinks />
        </nav>

        <nav className="header__nav">
          <NavLinks />
        </nav>

        <GetStartedButton href="/" />
      </header>
      <style jsx>
        {`
          :global(body) {
            overflow-y: ${drawerState ? "hidden" : "auto"};
          }
        `}
      </style>
    </>
  );
};

export default Navbar;
