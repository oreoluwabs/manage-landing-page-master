import Image from "next/image";
import Link from "next/link";
import { useEffect, useRef, useState } from "react";
import GetStartedButton from "../components/Button/GetStartedButton";
import Head from "../components/Head";
import styles from "../styles/modules/home.module.scss";
import { testimonials, whatsDifferent } from "../components/data";

const HomePage = () => {
  const [scrollProgress, setScrollProgress] = useState(0);
  const scrollTarget = useRef();

  useEffect(() => {
    scrollTarget.current.addEventListener("scroll", scrollListener);
    return () =>
      scrollTarget.current?.removeEventListener("scroll", scrollListener);
  });

  const scrollListener = () => {
    if (!scrollTarget.current) return;

    const element = scrollTarget.current;
    const windowScroll = element.scrollLeft;
    const totalWidth = element.scrollWidth - element.clientWidth;

    if (windowScroll === 0) return setScrollProgress(0);
    if (windowScroll > totalWidth) return setScrollProgress(100);

    setScrollProgress((windowScroll / totalWidth) * 100);
  };

  return (
    <>
      <Head title="Manage Landing Page | Home" />

      {/* Home Page */}
      <main className={`${styles.hero} ${styles.section} container`}>
        <div>
          <Image
            src="/images/illustration-intro.svg"
            alt="advantages illustration"
            width={580}
            height={525}
          />
        </div>
        <div>
          <h1 className={styles.hero__title}>
            Bring everyone together to build better products.
          </h1>
          <p className={styles.hero__subtitle}>
            Manage makes it simple for software teams to plan day-to-day tasks
            while keeping the larger team goals in view.
          </p>
          <GetStartedButton className={styles["get-started__btn"]} />
        </div>
      </main>

      {/* Whats Different */}
      <section className={`${styles["features"]} ${styles.section} container`}>
        <div className={styles["features__intro"]}>
          <h2 className={styles.section__title}>
            What’s different about Manage?
          </h2>
          <p className={styles.section__subtitle}>
            Manage provides all the functionality your team needs, without the
            complexity. Our software is tailor-made for modern digital product
            teams.
          </p>
        </div>

        <div className={styles["features__items"]}>
          {whatsDifferent.map(({ title, details }, index) => {
            const count = index < 10 ? `0${index + 1}` : index + 1;
            return (
              <div className={styles["features__item"]} key={title + index}>
                <div className={styles["features__item__title"]}>
                  <span className={styles["title__count"]}>{count}</span>
                  <span className={styles["title"]}>{title}</span>
                </div>
                <p className={styles["features__item__details"]}>{details}</p>
              </div>
            );
          })}
        </div>
      </section>

      {/* What They've Said */}
      <section className={`${styles.testimonial__section} ${styles.section}`}>
        <h2 className={styles.section__title}>What they’ve said</h2>
        <div className={styles.testimonials} ref={scrollTarget}>
          {testimonials.map((item, index) => {
            return (
              <div className={styles.testimonial} key={item.name + index}>
                <div className={styles.testimonial__img}>
                  <Image
                    src={item.img}
                    width={80}
                    height={80}
                    alt={`${item.name}`}
                  />
                </div>
                <div className={styles.testimonial__author}>{item.name}</div>
                <p>{item.testimony}</p>
              </div>
            );
          })}
        </div>

        <div className={styles.testimonials__indicators}>
          {[...Array(testimonials.length).keys()].map((index) => {
            const selectedDotValue =
              (scrollProgress * testimonials.length) / 100;

            const isActive =
              selectedDotValue >= index && selectedDotValue <= index + 1;

            return (
              <div
                key={index}
                className={`${styles.testimonials__indicator} ${
                  isActive ? styles["testimonials__indicator-active"] : ""
                }`}
              />
            );
          })}
        </div>
        <GetStartedButton className={styles["get-started__btn"]} />
      </section>

      <section className={`${styles.banner} ${styles.section}`}>
        <div className={`${styles.banner__content} container`}>
          <h2 className={styles.section__title}>
            Simplify how your team works today.
          </h2>
          <Link href="/">
            <a
              className={`get-started__btn ${styles["banner__get-started__btn"]}`}
            >
              Get Started
            </a>
          </Link>
        </div>
      </section>
    </>
  );
};

export default HomePage;
